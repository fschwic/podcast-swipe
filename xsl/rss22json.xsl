<?xml version="1.0" encoding="UTF-8"?>
<!--
 RSS2 to JSON v0.9
 
 XSLT to create HTML from Podcasts (RSS2 documents).
 Every item of the podcast should have a thumbnail defined (media:thumbnail).
 
 copyright 2014 Frank Schwichtenberg <http://frank.schwichtenberg.net>
 
 You may freely copy, use, and modify this XSLT if you don't remove my name
 and the origin location of this XSLT.
 
 The origin location of this XSLT is
 http://frank.schwichtenberg.net/xsl/rss22json.xsl
 Here you may find new versions.
 -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:pdccs="http://schwichtenberg.net/pdccs/2008/" 
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rss="http://purl.org/rss/1.0/" xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:cv="http://3iii.net/cv">
  <xsl:output encoding="utf-8" indent="yes" method="text" />

  <xsl:template match="/rss"><xsl:apply-templates select="channel"/></xsl:template>



  <xsl:template name="string-replace-all">
  <xsl:param name="s" />
  <xsl:param name="pattern" />
  <xsl:param name="replacement" />
  <xsl:choose>
    <xsl:when test="contains($s, $pattern)">
      <xsl:value-of select="substring-before($s,$pattern)" />
      <xsl:value-of select="$replacement" />
      <xsl:call-template name="string-replace-all">
        <xsl:with-param name="s" select="substring-after($s,$pattern)" />
        <xsl:with-param name="pattern" select="$pattern" />
        <xsl:with-param name="replacement" select="$replacement" />
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$s" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

  <xsl:template name="json-string">
    <xsl:param name="node"/>
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="s">
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="s" select="normalize-space($node/text())"/>
          <xsl:with-param name="pattern">"</xsl:with-param>
          <xsl:with-param name="replacement">\"</xsl:with-param>
        </xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="pattern">\|</xsl:with-param>
      <xsl:with-param name="replacement"></xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="channel">
{
"title":"<xsl:call-template name="json-string">
          <xsl:with-param name="node" select="title"/>
        </xsl:call-template>",
<xsl:if test="guid">"id":"<xsl:call-template name="json-string">
          <xsl:with-param name="node" select="guid"/>
        </xsl:call-template>",</xsl:if>
<xsl:if test="image/url">"image":"<xsl:value-of select="image/url"/>",
</xsl:if>
<!--
	  <xsl:if test="image">
	    <a border="0">
	      <xsl:attribute name="href"><xsl:value-of select="image/link"/></xsl:attribute>
	      <img class="img-rounded">
		<xsl:attribute name="src"><xsl:value-of select="image/url"/></xsl:attribute>
		<xsl:attribute name="alt"><xsl:value-of select="image/title"/></xsl:attribute>
	      </img>
	    </a>
	  </xsl:if>
  -->
<xsl:if test="description">"description":"<xsl:call-template name="json-string">
          <xsl:with-param name="node" select="description"/>
        </xsl:call-template>",</xsl:if>
<xsl:if test="managingEditor">"managingEditor":"<xsl:call-template name="json-string">
          <xsl:with-param name="node" select="managingEditor"/>
        </xsl:call-template>",</xsl:if>
<xsl:if test="pubDate">"pupDate":"<xsl:value-of select="pubDate"/>",</xsl:if>
<xsl:apply-templates select="atom:link[@rel = 'self']"/>
"items":[<xsl:apply-templates select="item"/>]
<!-- by date time
	      <xsl:variable name="MONTHES" select="document('datetime.xml')/datetime"/>
		<!- TODO very expensive! Use an appropriate datetime string in ContentCasts and choose if available. ->
	      <xsl:apply-templates select="item">
		<xsl:sort select="concat(substring(pubDate,13,4),$MONTHES/month[@abbr=substring(current()/pubDate,9,3)]/@nn,substring(pubDate,6,2),substring(pubDate,18,8))"/>
	      </xsl:apply-templates>
      -->
}</xsl:template>
  
  <xsl:template match="item">
    {
      <xsl:if test="guid">"id":"<xsl:value-of select="guid"/>",
</xsl:if>
      <xsl:if test="toc">"toc":{
          "index":"<xsl:value-of select="toc/@index"/>",
          "label":"<xsl:value-of select="toc/@label"/>"
        },
</xsl:if>
      "thumbnail":<xsl:choose>
        <xsl:when test="media:thumbnail">
          <xsl:apply-templates select="media:thumbnail[1]" />
	       </xsl:when>
	       <xsl:otherwise>
	         <!-- enclosure with smalles size -->
          <xsl:apply-templates select="enclosure[not(@length &lt; ../enclosure/@length)]"/>
	       </xsl:otherwise>
      </xsl:choose>,
      "enclosure":<xsl:apply-templates select="enclosure"/>,
<!--
	<xsl:choose>
	  <xsl:when test="link">
	    <a>
	      <xsl:attribute name="href">
		<xsl:value-of select="normalize-space(link)"/>
	      </xsl:attribute>
	      <xsl:value-of select="title"/>
	    </a>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="title"/>
	  </xsl:otherwise>
	</xsl:choose>
-->
      
      <!-- date -->
<!--      <xsl:if test="pubDate">
	<p class="pubDate"><xsl:value-of select="pubDate"/></p>
      </xsl:if>
      <xsl:if test="*[local-name() = 'creator']">
	<p class="creator"><xsl:value-of select="*[local-name() = 'creator']"/></p>
      </xsl:if>
-->
      <!-- description -->
      "description":"<xsl:choose>
      <xsl:when test="content:encoded">
        <xsl:call-template name="json-string">
          <xsl:with-param name="node" select="content:encoded"/>
        </xsl:call-template>
	    </xsl:when>
	    <xsl:when test="description">
        <xsl:call-template name="json-string">
          <xsl:with-param name="node" select="description"/>
        </xsl:call-template>
      </xsl:when>
      </xsl:choose>",

      <xsl:if test="*[local-name() = 'abstract']">"abstract":"<xsl:call-template name="json-string">
          <xsl:with-param name="node" select="*[local-name() = 'abstract']"/>
        </xsl:call-template>",</xsl:if>
      "title":"<xsl:call-template name="json-string">
          <xsl:with-param name="node" select="title"/>
        </xsl:call-template>"
    }<xsl:if test="position() != last()">,</xsl:if>
  </xsl:template>
  

  <xsl:template match="media:thumbnail">
"<xsl:value-of select="@url"/>"
  </xsl:template>

  <xsl:template match="enclosure">
    {
      "type":"<xsl:value-of select="@type"/>",
      "url":"<xsl:value-of select="@url"/>",
      "length":"<xsl:value-of select="@length"/>"
    }
  </xsl:template>

</xsl:stylesheet>
