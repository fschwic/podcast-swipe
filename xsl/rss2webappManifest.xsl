<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:pdccs="http://schwichtenberg.net/pdccs/2008/" 
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rss="http://purl.org/rss/1.0/" xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:xlink="http://www.w3.org/1999/xlink">
  <xsl:output encoding="utf-8" indent="yes" method="text" />

  <xsl:template match="/rss">
    <xsl:apply-templates select="channel"/>
  </xsl:template>

  <xsl:template match="channel">
{
  "name":"<xsl:value-of select="title"/>",
  "description":"<xsl:value-of select="description"/>",
  "launch-path":"url",
  "icons":{
    "128":"<xsl:value-of select="image/url"/>"
  }
}
  </xsl:template>

</xsl:stylesheet>
